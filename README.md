# Technical interview your art
## Time allowed : 1h30 (max) Start at 7h45

Steps

- Fetch
- Navigation
- Components
- Pages
- 
## Installation

Dillinger requires [Node.js](https://nodejs.org/) v16+ to run.

Install the dependencies and devDependencies and start the server.

```
yarn
yarn start
```

## Author

Alexendre OBLi
