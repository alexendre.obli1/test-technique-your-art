import React from 'react'
import ProductImage from '../atoms/image/productImage'
import { IconContext } from 'react-icons';
import { AiOutlineEye } from 'react-icons/ai';
import { MdText } from '../atoms/texts/title';
import { TbAugmentedReality } from 'react-icons/tb';
import Accordion from '../organisms/accordion';

type Props = {
  imageUrl?: string
  description?: string
  subjects?: string[]
  styles?: string[]
  mediums?: string[]
  materials?: string[]
}

export default function ProductArtDetails({ imageUrl, description, subjects, styles, mediums, materials }: Props) {

  let aAccordionSections = [
    {
      title: 'Description',
      description: [
        {
          heading: 'description',
          withHeading: false,
          content: description,
        },
      ],
    },
    {
      title: 'Subject, Medium, Style, Materials',
      description: [
        {
          heading: 'Subject',
          withHeading: true,
          content: subjects?.join(','),
        },
        {
          heading: 'Styles',
          withHeading: true,
          content: styles?.join(','),
        },
        {
          heading: 'Mediums',
          withHeading: true,
          content: mediums?.join(','),
        },
        {
          heading: 'Materials',
          withHeading: true,
          content: materials?.join(','),
        },
      ],
    },
  ]

  return (
    <>
      <ProductImage src={imageUrl} />
      <div
        className="spacing"
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          gap: '20px',
        }}
      >
        <div
          onClick={() => {
            alert("Not implemented")
          }}
          style={{ display: 'flex', gap: '10px', alignItems: 'center', cursor: "pointer" }}
        >
          <IconContext.Provider
            value={{
              color: 'black',
              size: '30',
              style: { verticalAlign: 'middle' },
            }}
          >
            <div>
              <AiOutlineEye />
            </div>
          </IconContext.Provider>
          <MdText style={{ whiteSpace: 'nowrap' }}>VIEW IN A ROOM</MdText>
        </div>

        <div
          onClick={() => {
            alert("Not implemented")
          }}
          style={{ display: 'flex', gap: '10px', alignItems: 'center', cursor: "pointer" }}
        >
          <IconContext.Provider
            value={{
              color: 'black',
              size: '30',
              style: { verticalAlign: 'middle' },
            }}
          >
            <div>
              <TbAugmentedReality />
            </div>
          </IconContext.Provider>
          <MdText style={{ whiteSpace: 'nowrap' }}>AR VIEW</MdText>
        </div>
      </div>
      <div className="spacing">
        <Accordion
          sections={aAccordionSections}
        />
      </div>
    </>

  )
}