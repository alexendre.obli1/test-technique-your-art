
import { IconContext } from 'react-icons';
import { AiOutlineStar } from 'react-icons/ai';
import { BsCheckLg, BsPinMap } from 'react-icons/bs';
import { HiTruck } from 'react-icons/hi';
import SpacingHolder from '../layout/spacingHolder.layout';
import Col from '../layout/column.layout';
import Row from '../layout/row.layout';
import CTAButton from '../atoms/buttons/CTA';
import { HighlighText, InfoText, MainTitle, MdText, SmText, SubTitle, XsText } from '../atoms/texts/title';
import { FaHourglassHalf } from 'react-icons/fa';

type Props = {

  title: string | null,
  artistShort: { fullname?: string | null, country?: string | null }
  category: string | null
  creationYear: string | null
}

export default function ProductInformations({ title, artistShort, category, creationYear }: Props) {
  return (
    <div
      className="col sm-col-4"
      style={{
        flex: 0.4,
      }}
    >
      <SpacingHolder spacing={10}>
        <div
          style={{
            alignItems: 'center',
            justifyContent: 'space-between',
            display: "flex",
            width: "100%"
          }}
        >
          <Col flex={0.5}>
            <MainTitle>{title}</MainTitle>
          </Col>
          <div style={{ marginLeft: "auto" }}>
            <IconContext.Provider
              value={{
                color: 'black',
                size: '30',
                style: { verticalAlign: 'middle' },
              }}
            >
              <div>
                <AiOutlineStar />
              </div>
            </IconContext.Provider>
          </div>
        </div>
        <Row>
          <Col flex={0.4}>
            <SubTitle style={{ color: 'orange' }}>
              {artistShort?.fullname}
            </SubTitle>
          </Col>
          <Col flex={0.2}>
            <SubTitle style={{ color: 'lightgray' }}>
              {artistShort?.country}
            </SubTitle>
          </Col>
        </Row>

        <Row>
          <Col flex={0.5}>
            <InfoText>{category}</InfoText>
          </Col>
          <Col flex={0.5}>
            <InfoText>{creationYear}</InfoText>
          </Col>
        </Row>
      </SpacingHolder>
      <div className="spacing">
        <HighlighText>3,810 €</HighlighText>
      </div>

      <SpacingHolder spacing={10}>
        <CTAButton
          variant={'dark'}
          text={'Acquire'}
          onClick={function (): void {
            throw new Error('Function not implemented.');
          }}
        />
        <CTAButton
          variant={'light'}
          text={'Make an offer'}
          onClick={function (): void {
            throw new Error('Function not implemented.');
          }}
        />
        <div className="row" style={{ alignItems: 'center' }}>
          <Col flex={0.2}>
            <IconContext.Provider
              value={{
                color: 'black',
                size: '30',
                style: { verticalAlign: 'middle' },
              }}
            >
              <div>
                <FaHourglassHalf />
              </div>
            </IconContext.Provider>
          </Col>
          <Col flex={0.8}>
            <MdText>PRE-RESERVE FOR 24 HOURS</MdText>
          </Col>
        </div>
        <div className="row" style={{ alignItems: 'center' }}>
          <Col flex={0.2}>
            <IconContext.Provider
              value={{
                color: 'black',
                size: '20',
                style: { verticalAlign: 'middle' },
              }}
            >
              <div>
                <BsCheckLg />
              </div>
            </IconContext.Provider>
          </Col>
          <Col flex={0.5}>
            <XsText>131€ Astimalad dalivary fas lor FrAncA</XsText>
          </Col>
        </div>
      </SpacingHolder>

      <MdText style={{ fontWeight: 'bold', marginBottom: '30px' }}>
        In order to obtein on accurate de very tee. please enter your
        country of residence and zip code
      </MdText>

      <SpacingHolder spacing={10}>
        <div
          className="row"
          style={{ alignItems: 'center', gap: '10px' }}
        >
          <Col flex={0.25}>
            <input className="form-control" placeholder="Spain" />
          </Col>
          <Col flex={0.25}>
            <input className="form-control" placeholder="8192" />
          </Col>
        </div>
        <div
          className="row"
          style={{ alignItems: 'center', gap: '10px' }}
        >
          <Col flex={0.2}>
            <IconContext.Provider
              value={{
                color: 'black',
                size: '20',
                style: { verticalAlign: 'middle' },
              }}
            >
              <div>
                <HiTruck />
              </div>
            </IconContext.Provider>
          </Col>
          <Col flex={0.8}>
            <SmText>Delivery fee is 129 €</SmText>
          </Col>
        </div>
      </SpacingHolder>
      <div className="spacing">
        <SpacingHolder spacing={10}>
          <div
            className="row"
            style={{ alignItems: 'center', gap: '10px' }}
          >
            <Col flex={0.2}>
              <IconContext.Provider
                value={{
                  color: 'black',
                  size: '20',
                  style: { verticalAlign: 'middle' },
                }}
              >
                <div>
                  <BsPinMap />
                </div>
              </IconContext.Provider>
            </Col>
            <Col flex={0.8}>
              <SmText>Free pickup in Bruelles Belgium</SmText>
            </Col>
          </div>
          <div
            className="row"
            style={{ alignItems: 'center', gap: '10px' }}
          >
            <Col flex={0.2}>
              <IconContext.Provider
                value={{
                  color: 'black',
                  size: '20',
                  style: { verticalAlign: 'middle' },
                }}
              >
                <div>
                  <BsCheckLg />
                </div>
              </IconContext.Provider>
            </Col>
            <Col flex={0.8}>
              <SmText>Try 14 devs et home for tres</SmText>
            </Col>
          </div>
        </SpacingHolder>
      </div>
    </div>
  )
}