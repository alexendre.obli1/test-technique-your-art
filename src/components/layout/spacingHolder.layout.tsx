import React, { ReactElement } from 'react';
import styled from 'styled-components';

interface ITextHolder {
  spacing: number;
  children: ReactElement | ReactElement[];
}
export default function SpacingHolder({ spacing, children }: ITextHolder) {
  return <Holder style={{ gap: spacing }}>{children}</Holder>;
}

const Holder = styled.div`
  display: flex;
  flex-direction: column;
  gap: ${(props) => props.style?.gap}px;
  width: 100%;
  align-items: flex-start;
`;
