import React, { ReactElement } from 'react';
interface ITemplates {
  children: ReactElement | ReactElement[];
  fluid: boolean;
}

export default function Container({ children, fluid }: ITemplates) {
  return (
    <div className={fluid ? 'container fluid' : 'container'}>{children}</div>
  );
}
