import React, { ReactElement } from 'react';
interface ITemplates {
  children: ReactElement | ReactElement[];
}

export default function Row({ children }: ITemplates) {
  return <div className="row">{children}</div>;
}
