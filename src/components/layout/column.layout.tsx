import React, { ReactElement } from 'react';
interface ITemplates {
  flex: number;
  children: ReactElement | ReactElement[];
}

export default function Col({ flex, children }: ITemplates) {
  return (
    <div className="column" style={{ flex }}>
      {children}
    </div>
  );
}
