import React from 'react';
import styled from 'styled-components';

interface IProductImage {
  src: string;
}

export default function ProductImage({ src }: IProductImage) {
  return (
    <Holder>
      <>{src && <ProductImageComponent src={src} />} </>
    </Holder>
  );
}

const Holder = styled.div`
  aspect-ratio: 16 / 9;
`;

const ProductImageComponent = styled.img`
  width: 100%;
  cursor: pointer;
  object-fit: contain;
  max-height: 50vh;
`;
