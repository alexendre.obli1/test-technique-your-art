import React from 'react';
import styled from 'styled-components';

interface ICTAButton {
  variant: 'dark' | 'light';
  text: string;
  onClick: () => void;
}

export default function CTAButton({ variant, text, onClick }: ICTAButton) {
  return (
    <ButtonStyle type="button" onClick={onClick} className={variant}>
      {text}
    </ButtonStyle>
  );
}

const ButtonStyle = styled.button`
  width: 100%;
  max-width: 375px;
  border: 1px solid black;
  border-radius: 20px;
  font-size: 32px;
  padding: 0px 0px;
  background-color: #fff;
  color: #000;
  cursor: pointer;
  transition-duration: 0.2s;

  &:hover {
    opacity: 0.7;
  }

  &.dark {
    background-color: #000;
    color: white;
  }
`;
