import styled from 'styled-components';

export const MainTitle = styled.h1`
  font-size: 16;
  font-weight: 600;
  color: black;
`;
export const SubTitle = styled.h2`
  font-size: 14;
  font-weight: 500;
`;

export const InfoText = styled.p`
  font-size: 12;
`;

export const HighlighText = styled.p`
  font-size: 30px;
  color: black;
  font-weight: 800;
`;

export const XsText = styled.p`
  font-size: 10px;
`;

export const MdText = styled.p`
  font-size: 14px:
`;

export const SmText = styled.p`
  font-size: 14px:
`;

export const LgText = styled.p`
  font-size: 18px:
`;
