import { useLocation } from 'react-router-dom';
import { IconContext } from 'react-icons';
import { FaChevronRight } from 'react-icons/fa';
interface IbreadCrumbs {
  title: string;
}

export default function BreadCrumbs({ title }: IbreadCrumbs) {
  const location = useLocation();
  return (
    <div style={{ display: 'flex', gap: '20px', marginBottom: '30px' }}>
      {location.pathname
        .split('/')
        .filter((path, index) => {
          return (
            path.length > 0 && index < location.pathname.split('/').length - 1
          );
        })
        .map((path) => {
          return (
            <div
              key={path}
              style={{
                display: 'flex',
                alignItems: 'center',
                gap: '10px',
                justifyContent: 'center',
              }}
            >
              <a
                href={`/${path}`}
                style={{ color: 'lightgray', textDecoration: 'none' }}
              >
                {path}
              </a>
              <IconContext.Provider
                value={{ color: 'black', style: { verticalAlign: 'middle' } }}
              >
                <div>
                  <FaChevronRight />
                </div>
              </IconContext.Provider>
            </div>
          );
        })}
      <a>{title}</a>
    </div>
  );
}
