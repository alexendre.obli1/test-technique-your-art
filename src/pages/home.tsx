import React from 'react';

export default function Home() {
  return (
    <div>
      <ol>
        <li>
          <a href="/artwork/0">first product</a>
        </li>
        <li>
          <a href="/artwork/1">second product</a>
        </li>
      </ol>
    </div>
  );
}
